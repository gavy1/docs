# Modifications apportées à Framapad

Au cours des mois d'avril et mai 2016, nous avons apporté plusieurs modifications
à l'éditeur collaboratif [Framapad](https://framapad.org).
Cette page vous présente ces modifications ainsi que la procédure à suivre pour les activer/désactiver.

Pour profiter de ces nouvelles options, vous aurez éventuellement besoin d'accéder à vos paramètres :

![](images/Selection_052.png)

## Réactivation des couleurs d'identification des auteurs.

Lorsque différents auteurs écrivent, chacun dispose de la couleur de surlignage de son choix.
Cependant, ce comportement peut parfois être gênant (un très grand nombre d'auteurs peut gêner la lisibilité, par exemple).

Au lieu d' “Effacer les couleurs identifiant les auteurs” (ce qui les supprime définitivement pour **tout le monde**),
il est possible de désactiver cette fonctionnalité simplement en allant dans vos paramètres.
Ainsi, les couleurs seront cachées sur votre navigateur mais disponibles pour les autres utilisateurs du pad.

Avant ![](images/Selection_050.png)

Après ![](images/Selection_051.png)

## Affichage du nom des auteurs au survol

Si vous avez désactivé l'option « Couleurs d'identification »,
vous apprécierez sûrement cette option : en survolant un texte,
le nom de l'auteur (s'il en a saisi un) apparaîtra pendant quelques secondes au-dessus du curseur.

![](images/Selection_061.png)

## Table des matières

Il est maintenant possible d'afficher une table des matières à droite de votre texte.

Les éléments de cette liste sont générés automatiquement à partir des **niveaux de titres de votre pad**

Un clic sur le titre dans la table des matières vous amènera directement à la section concernée.

Avant ![](images/Selection_055.png)

Après ![](images/Selection_056.png)

**ATTENTION** : la table des matière n'est **pas** mise à jour en temps réel.
Pour l'actualiser, il vous suffit de décocher puis cocher à nouveau l'option (oui, c'est un peu fastidieux, on sait…)

## Mode page

Un mode « page » a aussi été mis en place. Pour l'activer, cochez simplement l'option *Page View*.

![](images/Selection_062.png)

Ce mode permet une lecture plus aisée pour les textes longs.
Si l'option est activée, les sauts de page s'effectuent automatiquement (il n'est pas possible de les forcer à l'endroit de son choix).