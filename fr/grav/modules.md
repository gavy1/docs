# Modules

## Clients

![](images/clients.png)

``` markdown
[g-clients attributes="id:_clients,class:clients module"]

## Clients / Revue de presse / Références
Utilisez le module `clients` pour montrer à vos visiteurs par exemple quelles sont les clients qui vous font confiance ou bien quels sont les médias qui ont parlé de votre projet

___

[g-clients-item image="gnu.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="arch.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="debian.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="fedora.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="linux.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="mint.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="opensuse.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="ubuntu.png" attributes="class:col-md-3"][/g-clients-item]

[/g-clients]
```

## Portfolio

![](images/portfolio.png)

``` markdown
[g-portfolio attributes="id:_portfolio,class:portfolio module"]

## Galerie / Portfolio
Utilisez le module `portfolio` pour présenter votre travail.

___

[g-thumbnail]
[g-thumbnail-item image="coffee.jpg" url="/"][/g-thumbnail-item]
[g-thumbnail-item image="farmerboy.jpg" url="/"][/g-thumbnail-item]
[g-thumbnail-item image="girl.jpg" url="/"][/g-thumbnail-item]
[g-thumbnail-item image="judah.jpg" url="/"][/g-thumbnail-item]
[g-thumbnail-item image="origami.jpg" url="/"][/g-thumbnail-item]
[g-thumbnail-item image="retrocam.jpg" url="/"][/g-thumbnail-item]
[/g-thumbnail]

[/g-portfolio]
```

## Team

![](images/team.png)

``` markdown
[g-team attributes="id:_team,class:team module"]

## L’équipe
Utilisez le module `team` pour présenter vos collaborateurs, partenaires, employés, etc…

___

[g-team-item image="jane.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]

[g-team-item image="mark.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]

[g-team-item image="julia.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]
[/g-team]
```

## What we do

![](images/what_we_do.png)

``` markdown
[g-what-we-do name="what_we_do" attributes="class:what-we-do module" column_attributes="id:_what_we_do,class:col-md-12"]

## Le projet
Utilisez le module `what_we_do` pour expliquer à vos visiteurs votre projet, les activités de votre entreprise, les missions de votre association, etc…
___

[g-what-we-do-item attributes="class:col-md-4"]

<div class="item-icon">
[g-icon icon="bullhorn fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

[/g-what-we-do-item]

[g-what-we-do-item attributes="class:col-md-4"]
<div class="item-icon">
[g-icon icon="bolt fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

[/g-what-we-do-item]

[g-what-we-do-item attributes="class:col-md-4"]

<div class="item-icon">
[g-icon icon="heart fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
[/g-what-we-do-item]

[/g-what-we-do]
```

## Footer

![](images/footer.png)

``` markdown
[g-footer-one name="footer" render=false]
[g-section name="credits"]

Ce site est motorisé par [Grav CMS](http://getgrav.org/) avec le theme [Gravstrap](http://diblas.net/themes/gravstrap-theme-to-start-grav-cms-site-with-bootstrap-support/) adapté par [Framasoft](https://framasoft.org/)

[/g-section]
[/g-footer-one]
```

## Images collage

![](images/collage.png)

``` markdown
[g-images-collage columns="4" border="3" width="800" attributes="title:Image title,class:img-responsive"][/g-images-collage]
```