# Les types de votes dans Framavox

Nous poursuivons notre [exemple factice](exemple-d-utilisation.html) suite à une mise à jour du logiciel Loomio, qui apporte enter autres nouveautés de nouveaux types de votes pour mieux se décider en équipe&nbsp;!

<h2>Le G.A.G. organise son festival «&nbsp;Le Gras, c'est la vie&nbsp;!&nbsp;»</h2>

Sandrine est ravie&nbsp;: <a href="exemple-d-utilisation.html">depuis que le G.A.G. s'est mis à Framavox</a>, on peut recentrer les discussions, ne plus perdre de temps dans d'interminables réunions et prendre des décisions horizontales et équitables en tenant compte des voix de chacun·e.

Lors de leur dernière rencontre, le G.A.G. a décidé d'organiser un festival à haute teneur en lipides et délices<em> grassieux</em>. Sandrine se dit que c'est l'occasion rêvée de tester les nouvelles fonctionnalités de Framavox. Elle se rend donc sur <a href="https://framavox.org">Framavox.org</a> et se connecte avec son email et son mot de passe, puis elle clique dans la colonne de gauche pour se rendre sur la page des discussions du G.A.G.

![framavox nouveaux détails](images/vox-new-details.png)
&nbsp;

Déjà, l'interface est plus claire, épurée et ça, ça plaît à Sandrine qui n'aime pas être perdue dans un déluge d'informations. Par exemple, elle note que&hellip;
<ol>
 	<li>&hellip;il y a un bouton «&nbsp;hamburger&nbsp;», ou «&nbsp;menu&nbsp;» et si elle le clique ça&hellip;</li>
 	<li>&hellip;affiche ou ça masque la colonne de gauche, qui est claire et éloquente. Elle la masque, puis voit que&hellip;</li>
 	<li>&hellip;elle a trois notifications, sur des discussions précédentes.</li>
</ol>
Pressée de commencer l'organisation de ce nouveau festival, Sandrine va cliquer sur le bouton «&nbsp;<em>New decision</em>&nbsp;» (tiens, la traduction n'a pas encore été faite, se dit-elle, c'est donc que ça a dû changer&hellip;). Et là, au lieu de voir l'unique type de vote auquel elle avait droit, elle se retrouve face à 5 possibilités.

![Framavox Décisions](images/vox-decisions.png)

&nbsp;

<h3>La proposition</h3>

Sandrine commence par faire une proposition pour vérifier que le G.A.G. est d'accord pour organiser ce festival. La proposition, elle connaît, c'était le vote qui existait déjà dans <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a>, qui permet d'exprimer un «&nbsp;Oui - Je m'abstiens - Non - Je Bloque le vote&nbsp;». Ça tombe bien, c'est justement de cela dont elle avait besoin pour débuter&nbsp;! Elle remplit donc le formulaire&nbsp;:

![Framavox vote proposition](images/vox-proposition-vote.png)

Puis elle va voir le résultat de son formulaire, afin d'aller donner son opinion.

![Framavox résultat proposition](images/vox-proposition-resultat.png)

&nbsp;

<h3>La vérification</h3>

Le projet a été accepté et Sandrine a besoin de savoir qui dans le groupe peut donner du temps sur l'organisation de ce festival, afin de préparer le comité qui travaillera dessus. Elle décide d'utiliser l'outil de vérification de <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a>.


![Framavox vote Vérification](images/vox-verification-vote.png)


Le résultat l'étonne&nbsp;: c'est simple, mais efficace&nbsp;! Elle se dit qu'elle pourra l'utiliser dans de nombreux cas&hellip;


![Framavox résultat Vérification](images/vox-verification-resultat.png)

&nbsp;

<h3>Le sondage</h3>

Quelques jours plus tard, il y a une décision importante à prendre. La mairie leur laisse le choix dans le lieu que le G.A.G. peut investir sur leur festival. La MJC, la salle des fêtes, la place du marché&hellip; Chaque choix a ses avantages et ses inconvénients et le G.A.G. les a déjà pesés dans une discussion <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a>. Sandrine se dit qu'il est l'heure de trancher (dans l'gras&nbsp;!), et décide de lancer un sondage.

![Framavox vote Sondage](images/vox-sondage-vote.png)

Là encore, elle apprécie de pouvoir simplement proposer un choix multiple, avec autant de cases qu'elle le voudrait. Il n'y a plus qu'à aller faire entendre sa voix&nbsp;:

![Framavox résultat Sondage](images/vox-sondage-resultat.png)

<h3>Le vote à points</h3>

Ça y est, le festival commence à se concrétiser, les sponsors et subventions sont là, le G.A.G. a donc un budget. C'est bien joli, mais suivant les priorités, on peut choisir de le répartir de multiples manières&hellip; Sandrine se dit que c'est l'occasion rêvée de tester le vote à points de <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a>&nbsp;: elle attribue 10 points à chaque membres, en leur expliquant que chacun de ces points représenterait 10 % du budget&hellip; Ainsi, en faisant la moyenne, elle saura où vont les priorités du G.A.G.

![Framavox vote Points](images/vox-points-vote.png)

Bien&nbsp;! Elle a hâte de voir ce que vont donner les résultats, mais avant cela, il lui faut aller attribuer ses 10 points à elle&nbsp;!

![Framavox résultats Points](images/vox-points-resultats.png)

<h3>Le sondage réunions</h3>

Le festival approche à grand pas&nbsp;! Le comité d'organisation doit donc vite se réunir pour faire le point et mettre en place les derniers préparatifs&hellip; Lourde tâche que de trouver une date de réunion qui convienne -_-&hellip; D'habitude, pour cela, Sandrine utilisait <a href="https://framadate.org">Framadate</a>, mais dans un groupe déjà établi sur <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a>, ce n'est pas ce qu'il y a de plus pratique. Elle est donc heureuse d'avoir accès à l'outil de sondage réunions&nbsp;:

![Framavox vote Date](images/vox-date-vote.png)

C'est sommaire et ça fera parfaitement l'affaire. Sandrine n'oublie pas d'aller voir le résultat pour donner ses disponibilités.

![Framavox résultat Date](images/vox-date-resultat.png)

La morale de cette histoire&nbsp;: pour un groupe déjà établi, <a href="https://framavox.org"><b class="violet">Frama</b><b class="vert">vox</b></a> (grâce à la puissance du logiciel Loomio) est devenu un outil complet&nbsp;: il est parfait pour cadrer chacune des discussions dans leur propre fil et proposer des votes adaptés en fonction de chaque type de décision que le groupe a à prendre. Et surtout, c'est là un outil qui permet à tou·te·s de faire entendre leur voix, donc d'éviter les jeux de hiérarchie et d'ego qui minent souvent un joyeux groupe comme le G.A.G.

Et si un groupe (factice ^^) dont la devise est «&nbsp;Le Gras, c'est la vie&nbsp;» n'a pas réussi à vous convaincre, ben il ne vous reste plus qu'à&hellip; tester par vous-même&nbsp;!
